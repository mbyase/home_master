/**
* 
* @brief	网络连接状态管理服务
* @author	yun
* @date		2016-12-29 13:09:51
* @version	0.1
* @desc		
*
**/

#include	"service_network.h"

#include	"platform_logger.h"
#include	"platform_button.h"

#include	"cmsis_os.h"

#include	"mxchipWNET.h"

#include	<string.h>

#include	"app_conf.h"


typedef enum{
	EVENT_NONE = 0,
	EVENT_WIFI_UP,     		// WIFI 连接
	EVENT_WIFI_DOWN,    	// WIFI 断开
	EVENT_WIFI_AIRKISS,    // AirKiss 设置
}network_event_typedef;

//
network_event_typedef	network_event;
network_InitTypeDef_st	network_conf;

static void thread_network(void const * argument);
static int airkiss_callback(void);

static osThreadId network_id = NULL;

// 建立网络连接
int network_establish(void){
	osThreadDef(Network, thread_network, osPriorityNormal, 0, 512);
	network_id = osThreadCreate(osThread(Network), NULL);
	if(network_id == NULL){
		return -1;
	}
	return 0;
}


//
int network_destroy(void){
	if(network_id != NULL){
		osThreadTerminate(network_id);
	}
	return 0;
}

//
static int airkiss_callback(void){
	network_event =  EVENT_WIFI_AIRKISS;
	return 0;
}

// 网络
static void thread_network(void const * argument){
	log_notice_printf("thread: network");

	// 建立默认的连接
	memset(&network_conf, 0x00, sizeof(network_conf));
	network_conf.wifi_mode = Station;
	strcpy((char *)network_conf.wifi_ssid, APP_WIFI_NAME);
	strcpy((char *)network_conf.wifi_key, APP_WIFI_PASSWORD);
	network_conf.dhcpMode = DHCP_Client;
	StartNetwork(&network_conf);
	
	//airkiss 返回连接
	platform_airkiss_btn(airkiss_callback);

	// 处理网络连接事件
	network_event = EVENT_NONE;
	while(1){
		mxchipTick();
		
		switch(network_event){
			case EVENT_NONE:
				break;
			
			case EVENT_WIFI_UP:
				app_state_notification(APP_NETWORK_CON);
				break;
			
			case EVENT_WIFI_DOWN:
				app_state_notification(APP_NETWORK_DISCON);
				break;
			
			case EVENT_WIFI_AIRKISS:
				{
					app_state_notification(APP_NETWORK_DISCON);
					wlan_disconnect();
					mxchipTick();
					log_notice_printf("start airkiss");
					OpenEasylink2_withdata(30);
				}
				break;
			
			default:
				break;
		}
		network_event = EVENT_NONE;
		
		osDelay(10);
	}
	
}


/**
* @brief 上海请客提供的回掉函数
*
*/
// WIFI 扫描结果
void ApListCallback(ScanResult *pApList){
	int i;
	log_notice_printf("Find %d APs: \n", pApList->ApNum);
	for (i=0;i<pApList->ApNum;i++)
		log_printf("\tSSID: %s, Signal: %d%%\r\n", pApList->ApList[i].ssid, pApList->ApList[i].ApPower);
}


void RptConfigmodeRslt(network_InitTypeDef_st *nwkpara){
	if(nwkpara == NULL){
		log_printf("    airkiss timeout\r\n");
	} else {
		memcpy(&network_conf, nwkpara, sizeof(network_InitTypeDef_st));
		log_printf("airkiss configuration:\n");
		log_printf("    ssid: %s\n", network_conf.wifi_ssid);
		log_printf("    password: %s\n", network_conf.wifi_key);
	}
}


//airkiss 用户数据输入
void easylink_user_data_result(int datalen, char* data){
   net_para_st para;
   getNetPara(&para, Station);
   if(!datalen){
	   log_notice_printf("user input : null\n");
   }else{
	   log_notice_printf("user input:  %s  len=%d\n", data, datalen);
   }
} 


// 网络连接成功后返回结果
void NetCallback(net_para_st *pnet){
	log_info_printf("network: {\"ip\": \"%s\", \"mac\":\"%s\"}", pnet->ip, pnet->mac);
}

// WIFI时间处理
void WifiStatusHandler(int event){
  switch (event) {
    case MXCHIP_WIFI_UP:
		network_event =  EVENT_WIFI_UP;
		break;
    case MXCHIP_WIFI_DOWN:
        network_event =  EVENT_WIFI_DOWN;
		break;
    default:
		break;
  }
  return;
}



