/**
*
* @brief	设备基本服务
* @author	yun
* @date		2017-01-20
*
* @desc		设备服务基本功能 [登入|登出|心跳]
* @version	v0.9
*
*/


#ifndef		__SERVICE_DEVICE_H
#define		__SERVICE_DEVICE_H


int service_device_establish(void);

int service_device_destroy(void);

int service_device_test(void);


#endif

