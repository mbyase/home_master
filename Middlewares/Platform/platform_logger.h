/***
*
* @brief		Logger
* @author		yun
* @date			2016-12-20 11:01:16
* @version		1.0.1
* @desc			
*
*/

#ifndef		__PLATFORM_LOGGER_H
#define		__PLATFORM_LOGGER_H

#include	<stdio.h>

typedef enum {
	PLATFORM_LOG_DEBUG = 0,
	PLATFORM_LOG_INFO,
	PLATFORM_LOG_NOTICE,
	PLATFORM_LOG_WARNING,
	PLATFORM_LOG_ERROR,
	PLATFORM_LOG_OFF			//	关闭日志输出
}platform_log_level_typedef;

void 	platform_log_init(platform_log_level_typedef log_level);

int		log_printf(const char *fmt, ...);

void 	platform_log_set_level(platform_log_level_typedef log_level);

extern 	platform_log_level_typedef		platform_log_level;

char * platform_get_cmd(void);


#define		log_debug_printf(format, ...)   \
{\
	if(platform_log_level <= PLATFORM_LOG_DEBUG) \
	{\
		log_printf("[debug] "format"\n", ##__VA_ARGS__);  \
	}\
}

#define		log_info_printf(format, ...)   \
{\
	if(platform_log_level <= PLATFORM_LOG_INFO) \
	{\
		log_printf("[info] "format"\n",##__VA_ARGS__);  \
	}\
}

#define		log_notice_printf(format, ...)   \
{\
	if(platform_log_level <= PLATFORM_LOG_NOTICE) \
	{\
		log_printf("[notice] "format"\n",##__VA_ARGS__);  \
	}\
}

#define		log_warning_printf(format, ...)   \
{\
	if(platform_log_level <= PLATFORM_LOG_WARNING) \
	{\
		log_printf("[warning] "format"\n",##__VA_ARGS__);   \
	}\
}

#define		log_error_printf(format, ...)   \
{\
	if(platform_log_level <= PLATFORM_LOG_ERROR) \
	{\
		log_printf("[error] "format"\n",##__VA_ARGS__);  \
	}\
}

int 	platform_log_test(void);


#endif

